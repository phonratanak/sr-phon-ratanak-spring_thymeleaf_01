package com.themeleafhomeword.article.repository;

import com.themeleafhomeword.article.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface articleRepository extends JpaRepository<Article,Integer> {
}
