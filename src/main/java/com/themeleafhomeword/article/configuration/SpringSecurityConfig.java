package com.themeleafhomeword.article.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/users").hasAnyRole("USER","ADMIN","EDITOR","REVIEWER")
                .antMatchers("/admin/editor").hasAnyRole("ADMIN","EDITOR","REVIEWER")
                .antMatchers("/admin/updatearticles/**").hasAnyRole("ADMIN","EDITOR")
                .antMatchers("/admin/reviewer").hasAnyRole("ADMIN","REVIEWER")
                .antMatchers("/admin/article").hasAnyRole("ADMIN","EDITOR")
                .antMatchers("/admin/review-articles").hasAnyRole("ADMIN","REVIEWER","EDITOR")
                .antMatchers("/admin/**").hasAnyRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/admin/login")
                .permitAll()
                .successForwardUrl("/admin/dashboard")
                .and()
                .logout()
                .permitAll()
                .logoutRequestMatcher(new AntPathRequestMatcher("/admin/logout"))
                .logoutSuccessUrl("/admin/login");
    }

    // create two users, admin and user
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
                .withUser("dara").password(passwordEncoder().encode("dara123")).roles("USER")
                .and()
                .withUser("makara").password(passwordEncoder().encode("makara123")).roles("ADMIN")
                .and()
                .withUser("kanha").password(passwordEncoder().encode("kanha123")).roles("EDITOR")
                .and()
                .withUser("reksmey").password(passwordEncoder().encode("reksmey123")).roles("REVIEWER");
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
