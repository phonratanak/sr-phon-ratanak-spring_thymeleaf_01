package com.themeleafhomeword.article.controller;

import com.themeleafhomeword.article.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class ArticleController {

    @Autowired
    com.themeleafhomeword.article.repository.articleRepository articleRepository;

    @GetMapping("/admin/article")
    public String postArticle(@ModelAttribute Article article , ModelMap modelMap)
    {
        modelMap.addAttribute("article", article);
        return "page/addArticle";
    }

    @GetMapping("/admin/review-articles")
    public String getArticle( ModelMap modelMap)
    {
        try {
            List<Article> articles= (List<Article>) articleRepository.findAll();
            modelMap.addAttribute("articles",articles);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return "/page/review-articles";
    }

    @PostMapping("/admin/article")
    public String addArticle(@ModelAttribute @Valid Article article, BindingResult result)
    {
        try {
            if(result.hasErrors()){
                return "redirect:/admin/postarticles";
            }
            articleRepository.save(article);

        }catch (Exception e)
        {
            System.out.println("Server error");
        }
        return "redirect:/admin/review-articles";
    }

    @GetMapping("/admin/deletearticles/{id}")
    @PreAuthorize("'ROLE_ADMIN'")
    public String deleteArticle(@PathVariable int id)
    {
        articleRepository.deleteById(id);
        return "redirect:/admin/review-articles";
    }

    @PostMapping("/admin/updatearticles/{id}")
    public String updateArticle(@ModelAttribute @Valid Article article,BindingResult result,@PathVariable int id)
    {
        Article article1=articleRepository.getOne(id);
        if(result.hasErrors()){
            return "redirect:/page/updateArticle";
        }
        articleRepository.save(article);


        return "redirect:/admin/review-articles";
    }

    @GetMapping("/admin/updatearticles/{id}")
    public String updateArticle(ModelMap modelMap,@PathVariable int id)
    {
       Article article1=articleRepository.getOne(id);

        modelMap.addAttribute("article", article1);
        return  "page/updateArticle";
    }

    @GetMapping("/admin/login")
    public String loginArticle()
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null || authentication instanceof AnonymousAuthenticationToken)
        {
            return "/page/login";
        }
        return "redirect:/page/dashboard";
    }

    @PostMapping("/admin/dashboard")
    public String dashbords()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER")))
        {
            return "redirect:/admin/users";
        }
        if(auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_EDITOR")))
        {
            return "redirect:/admin/editor";
        }
        if(auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_REVIEWER")))
        {
            return "redirect:/admin/reviewer";
        }
        return "redirect:/admin/dashboard";
    }

    @GetMapping("/admin/dashboard")
    public String dashbord()
    {
        return "page/dashboard";
    }

    @GetMapping("/admin/users")
    public String userPage()
    {
        return "page/userpage";
    }

    @GetMapping("/admin/editor")
    public String editorPage()
    {
        return "page/editorpage";
    }

    @GetMapping("/admin/reviewer")
    public String reviewerPage()
    {
        return "page/reviewerpage";
    }

    @GetMapping("/admin/logout")
    public String logout() {
        return "redirect:/page/login";
    }
}
